libparse-debianchangelog-perl (1.2.0-14) UNRELEASED; urgency=medium

  [ gregor herrmann ]
  * Update German translation.
    Thanks to Helge Kreutzmann for the improved po file.
    (Closes: #918553)

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Mon, 07 Jan 2019 16:33:08 +0100

libparse-debianchangelog-perl (1.2.0-13) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/rules: replace dpkg-parsechangelog call with /usr/share/dpkg
    /pkg-info.mk.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * Fix spelling error in patch description

  [ intrigeri ]
  * New patch: Fix-typo-in-POD.patch
  * Declare compliance with Debian Policy 4.2.1.
  * Bump debhelper compatibility level to 11.

 -- intrigeri <intrigeri@debian.org>  Mon, 29 Oct 2018 20:17:54 +0000

libparse-debianchangelog-perl (1.2.0-12) unstable; urgency=medium

  * Team upload
  * Document parsechangelog fails on version numbers not present in
    the changelog
  * Bump the policy version number in parsechangelog manpage
  * Fix erroneous escaping and formatting in Parse::DebianChangelog
    documentation. Closes: #848274

 -- Christoph Biedl <debian.axhn@manchmal.in-ulm.de>  Fri, 16 Dec 2016 20:02:18 +0100

libparse-debianchangelog-perl (1.2.0-11) unstable; urgency=medium

  * Team upload.
  * Remove Frank Lichtenheld from Uploaders. Thanks for your work!
  * New patch: don't trim file names in error messages.
    Thanks to Christoph Biedl for the bug report. (Closes: #840099)

 -- gregor herrmann <gregoa@debian.org>  Sat, 08 Oct 2016 15:26:40 +0200

libparse-debianchangelog-perl (1.2.0-10) unstable; urgency=medium

  * Team upload.
  * Add patch for compatibility with dpkg 1.18.8 which outputs a Timestamp
    field. (Closes: #829668)
  * Bump build dependency on dpkg-dev to 1.18.8.

 -- gregor herrmann <gregoa@debian.org>  Wed, 13 Jul 2016 18:26:38 +0200

libparse-debianchangelog-perl (1.2.0-9) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Add patch to update HTML template to avoid tidy errors in the tests.
    (Closes: #829064)
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Thu, 30 Jun 2016 11:26:12 +0200

libparse-debianchangelog-perl (1.2.0-8) unstable; urgency=medium

  * Team upload.
  * Set TZ=UTC for the whole build (again) to make the build reproducible.

 -- Niko Tyni <ntyni@debian.org>  Tue, 25 Aug 2015 23:38:45 +0300

libparse-debianchangelog-perl (1.2.0-7) unstable; urgency=medium

  * Team upload.
  * Further fiddling with TZ, aiming for reproducibility.

 -- Niko Tyni <ntyni@debian.org>  Mon, 24 Aug 2015 21:30:10 +0300

libparse-debianchangelog-perl (1.2.0-6) unstable; urgency=medium

  * Team upload.
  * Set TZ=UTC for the whole build to make the build reproducible.

 -- Niko Tyni <ntyni@debian.org>  Thu, 20 Aug 2015 21:41:46 +0300

libparse-debianchangelog-perl (1.2.0-5) unstable; urgency=medium

  * Team upload.
  * Add patch to remove support for broken timezone names.
    Thanks to Guillem Jover for the bug report and the patch.
    (Closes: #792414)

 -- gregor herrmann <gregoa@debian.org>  Tue, 14 Jul 2015 19:14:09 +0200

libparse-debianchangelog-perl (1.2.0-4) unstable; urgency=medium

  * Team upload.
  * Add explicit (build) dependency on libcgi-pm-perl.

 -- gregor herrmann <gregoa@debian.org>  Fri, 12 Jun 2015 21:25:50 +0200

libparse-debianchangelog-perl (1.2.0-3) unstable; urgency=medium

  * Run dh_auto_build with UTC timezone: otherwise, the local timezone
    impacts the (fixed) timestamps in .pot/.po/.mo files, and hence
    the build is not reproducible accross different timezones.
  * debian/control: include module name in the long description,
    and rephrase it a bit.
  * Move libmodule-build-perl from Build-Depends-Indep to Build-Depends:
    it is used in the 'clean' target.
  * Add build-dependency on the dpkg-dev version that introduced
    dpkg-parsechangelog's --show-field option, that is now used in debian/rules.

 -- intrigeri <intrigeri@debian.org>  Mon, 25 May 2015 14:13:39 +0200

libparse-debianchangelog-perl (1.2.0-2) unstable; urgency=medium

  * Adopt under the umbrella of the Debian Perl Group. Thanks to Frank
    Lichtenheld for his work!
    - Move Frank Lichtenheld to Uploaders, add myself there.
    - debian/control: update Vcs-* control fields.
  * Bump dh compatibility level to 9, and accordingly adjust
    the build-dependency on debhelper.
  * debian/copyright:
    - Convert to machine-readable copyright format 1.0.
    - Add information about files that have specific copyright provisions.
  * Add Homepage control field.
  * Declare compliance with Standards-Version 3.9.6.
  * Replace "perl (>= 5.10.1) | libtest-simple-perl (>= 0.88)" build-dependency
    with one on perl.
  * Reformat debian/control with cme.
  * Make the package build reproducibly:
    - Build.PL-when-a-PO_BUILD_DATE-environment-variable-i.patch: new patch,
      that makes the build system fake the time for running
      po4a-{translate, updatepo}, xgettext, msgmerge and msgfmt,
      when a PO_BUILD_DATE environment variable is passed.
    - debian/rules: export a PO_BUILD_DATE environment variable, set to the
      time of the last entry in debian/changelog.
    - Build-depend on faketime and libstring-shellquote-perl,
      that are needed by the PO_BUILD_DATE support in Build.PL.
  * debian/patches/*:
    - Add DEP-3 headers.
    - Rename to drop numbers (that were all 0001 anyway).
  * Mark package as autopkgtest-able, and declare additional files
    that are needed for the test suite to pass.

 -- intrigeri <intrigeri@debian.org>  Sun, 24 May 2015 17:59:06 +0200

libparse-debianchangelog-perl (1.2.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * 0001_ftbfs_changes_line.patch: fix test failures due to changed Changes
    field, patch by Jordan Metzmeier (closes: #753034).
  * 0001_cve_format.patch: Fix CVE parsing for new format (closes: #731436).
  * 0001_de.po.patch: Updated German translations, thanks Hendrik Knackstedt
    (closes: #709626).
  * 0001_P-DC-Support-open-handles-as-input.patch: support open handles as
    input, patch by Niels Thykier (closes: #722896).
  * Merge ubuntu patch: downgrade libhtml-parser-perl, libhtml-template-perl,
    and libxml-simple-perl from Recommends to Suggests. This package is
    installed in minimal systems due to aptitude's (reasonable) dependency
    on it, but aptitude only uses rfc822 output.

 -- Thijs Kinkhorst <thijs@debian.org>  Thu, 11 Sep 2014 18:12:43 +0000

libparse-debianchangelog-perl (1.2.0-1) unstable; urgency=low

  * New upstream release:
    - Don't use $& or $` (Closes: #515018)
    - Allow '.' and '+' in distribution names. Patch by John Wright.
      (Closes: #565238)
    - Add Danish translation of bin.pot by Joe Dalton (Closes: #605820)
    - ChangesFilters:
      + Fix conversion of <http://something/> (Closes: #603341)
      + Allow to omit # before closed bug numbers (Closes: 446798 ;)
  * Bump Standards-Version to 3.9.1 (no changes)
  * Convert debian/rules to debhelper 7

 -- Frank Lichtenheld <djpig@debian.org>  Sun, 03 Apr 2011 23:25:41 +0200

libparse-debianchangelog-perl (1.1.1-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Filter duplicate bug numbers in Closes field. (Closes: #560634)
  * Localize $_ in for loop. (Closes: #584943)
    - Add build-dep on perl (>= 5.10.1) | libtest-simple-perl (>= 0.88)
      and libtest-exception-perl for new test.

 -- Ansgar Burchardt <ansgar@43-1.org>  Fri, 30 Jul 2010 19:01:12 +0900

libparse-debianchangelog-perl (1.1.1-2) unstable; urgency=low

  * Add liblocale-gettext-perl to Build-Depends-Indep: Fix FTBFS (LP:
    #179599). From Marco Rodrigues.
  * Remove XS- prefix from Vcs-* fields. dpkg supports them now.
  * Bump Standards-Version to 3.7.3 (no changes)
  * Build.PL: set create_packlist to 0 to avoid .packlist installation

 -- Frank Lichtenheld <djpig@debian.org>  Fri, 11 Jan 2008 11:48:05 +0100

libparse-debianchangelog-perl (1.1.1-1) unstable; urgency=low

  * New upstream release
    - Fixes handling of dates equal to the epoch (Closes: #440116)

 -- Frank Lichtenheld <djpig@debian.org>  Sat, 01 Sep 2007 03:25:06 +0200

libparse-debianchangelog-perl (1.1-1) unstable; urgency=low

  * New upstream release
    - Update Build-Depends-Indep
  * Bump debhelper compat level to 5
  * Bump Standards-Version to 3.7.2 (no changes)
  * Add XS-Vcs-{git,Browser}

 -- Frank Lichtenheld <djpig@debian.org>  Mon, 16 Jul 2007 04:18:36 +0200

libparse-debianchangelog-perl (1.0-1) unstable; urgency=low

  * New upstream release
    - Update Build-Depends-Indep and Depends

 -- Frank Lichtenheld <djpig@debian.org>  Wed, 12 Oct 2005 17:13:34 +0200

libparse-debianchangelog-perl (0.9-1) unstable; urgency=low

  * New upstream release

 -- Frank Lichtenheld <djpig@debian.org>  Tue,  4 Oct 2005 04:47:28 +0200

libparse-debianchangelog-perl (0.8-1) unstable; urgency=low

  * New upstream release
    - Update Depends
    - Update description and reformat it a bit to make it more readable

 -- Frank Lichtenheld <djpig@debian.org>  Mon,  3 Oct 2005 14:12:00 +0200

libparse-debianchangelog-perl (0.7-1) unstable; urgency=low

  * New upstream release
    - Update the description
    - Update Depends

 -- Frank Lichtenheld <djpig@debian.org>  Tue, 16 Aug 2005 02:40:39 +0200

libparse-debianchangelog-perl (0.6-1) unstable; urgency=low

  * New upstream release

 -- Frank Lichtenheld <djpig@debian.org>  Wed,  3 Aug 2005 14:02:26 +0200

libparse-debianchangelog-perl (0.5-1) unstable; urgency=low

  * New upstream release
    - Update the description

 -- Frank Lichtenheld <djpig@debian.org>  Tue,  5 Jul 2005 04:26:12 +0200

libparse-debianchangelog-perl (0.4-1) unstable; urgency=low

  * New upstream release
    - adapt Build-Depends-Indep and Recommends to new requirements
    - install new TODO file
    - install templates and CSS files to
      /usr/share/libparse-debianchangelog-perl/ and adapt the default
      paths in the module
    - fix some problems in the test scripts (ignore stderr garbage
      issued by dpkg-parsechangelog in pbuilder environment)

 -- Frank Lichtenheld <djpig@debian.org>  Tue,  5 Jul 2005 03:28:10 +0200

libparse-debianchangelog-perl (0.3a-1) unstable; urgency=low

  * Initial Release (Closes: #314559).

 -- Frank Lichtenheld <djpig@debian.org>  Fri,  1 Jul 2005 22:35:31 +0200

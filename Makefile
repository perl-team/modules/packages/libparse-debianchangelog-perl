verbose=0
installdirs=site

all: Build
	@./Build

Build: Build.PL
	perl Build.PL installdirs=$(installdirs)

install: Build
	@./Build install destdir=$(DESTDIR)

clean: Build
	@./Build realclean

dist: Build MANIFEST
	@./Build dist

MANIFEST: MANIFEST.SKIP Build
	@./Build manifest

stats: Build
	@./Build postats

test: Build
	@./Build test verbose=$(verbose)

.PHONY: all install clean dist stats check

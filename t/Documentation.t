# -*- perl -*-

use strict;
use warnings;

use Test::More;
use Test::Pod 1.00;

plan tests => 5+all_pod_files();

# don't use all_pod_files_ok since we can't plan() then previously
foreach (all_pod_files()) {
    pod_file_ok( $_, "POD $_ ok" );
}

use Test::Pod::Coverage;

foreach (qw( Parse::DebianChangelog
	     Parse::DebianChangelog::Entry
	     Parse::DebianChangelog::Util )) {
    pod_coverage_ok( $_, "POD coverage $_"  );
}

TODO: {
    local $TODO = "my documenation sucks";

    foreach (qw( Parse::DebianChangelog::ChangesFilters
		 Pod::UsageTrans )) {
	pod_coverage_ok( $_, "POD coverage $_"  );
    }
}
